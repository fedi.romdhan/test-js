'use strict';

/**
 * in-progress router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::in-progress.in-progress');
