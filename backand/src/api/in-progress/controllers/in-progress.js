'use strict';

/**
 * in-progress controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::in-progress.in-progress');
