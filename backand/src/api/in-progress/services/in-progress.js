'use strict';

/**
 * in-progress service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::in-progress.in-progress');
