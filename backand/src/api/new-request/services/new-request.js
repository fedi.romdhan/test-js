'use strict';

/**
 * new-request service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::new-request.new-request');
