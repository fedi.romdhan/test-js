'use strict';

/**
 * to-be-tested router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::to-be-tested.to-be-tested');
