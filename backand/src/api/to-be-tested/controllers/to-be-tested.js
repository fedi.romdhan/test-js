'use strict';

/**
 * to-be-tested controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::to-be-tested.to-be-tested');
