'use strict';

/**
 * to-be-tested service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::to-be-tested.to-be-tested');
