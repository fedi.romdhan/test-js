let id_project_active;
/*****************************************************************************
                    RENDER WINDOW OF TASK
 ****************************************************************************/
function create_noTask() {
  const el = document.createElement("div");
  el.className = "no-tasks";
  el.insertAdjacentHTML(
    "afterbegin",
    `<img src="assest/EmptyState.png" alt="">
    <div>
    <h3>No Tasks</h3>
    <p>Start creating your tasks</p>
  </div>
    <div class="no-task-btn">Create a new Task</div>`
  );
  document.querySelector(".project").appendChild(el);
  document.querySelector(".project-container").classList.add("hidden");
}

/*****************************************************************************
                          OPEN WINDOW TASK
 ****************************************************************************/
const open_task = function () {
  document.querySelector(".window-create-task").classList.remove("hidden");
  document.querySelector(".container").classList.add("container-blur");
};

const close_task = function () {
  document.querySelector(".window-create-task").classList.add("hidden");
  document.querySelector(".container").classList.remove("container-blur");
};
const close_task_update = function () {
  document.querySelector(".window-update-task").classList.add("hidden");
  document.querySelector(".container").classList.remove("container-blur");
};

document.querySelectorAll(".add-task").forEach((task) => {
  task.addEventListener("click", () => {
    open_task();
  });
});
/*****************************************************************************
                          OPEN WINDOW UPDATE TASK
 ****************************************************************************/
const open_update_task = function () {
  document.querySelector(".window-update-task").classList.remove("hidden");
  document.querySelector(".container").classList.add("container-blur");
  document.querySelector('.tag-principal').addEventListener('click',()=>{
    console.log('hello my world');
  
  })
  };

                          
const close_update_task = function () {
  document.querySelector(".window-update-task").classList.add("hidden");
  document.querySelector(".container").classList.remove("container-blur");
   };
/****************************************************************************
                            CLOSE WINDOW OF update TASK
*****************************************************************************/
document.querySelector(".cancel").addEventListener("click", () => {
  close_task();
});
document.querySelector(".close-icon").addEventListener("click", () => {
  close_task();
});
document.querySelector(".cancel-update").addEventListener("click", () => {
  close_task_update();
});
document.querySelector(".close-icon-update").addEventListener("click", () => {
  close_task_update();
});

/*************************************************************************
                        ADD TASK IN PROJECT TASKS
**************************************************************************/
async function add_task_inProject(idProject, allTask) {
  console.log(allTask);
  try {
    jwtToken = localStorage.getItem("token");

    const response = await fetch(
      `http://localhost:1337/api/projects/${idProject}?populate=deep,4`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${jwtToken}`,
        },
        body: JSON.stringify({
          data: { tasks: allTask },
        }),
      }
    );

    console.log(id_task_added, idProject, allTask, "oui");
    if (!response.ok) {
      throw new Error("Failed to register project");
    }
  } catch (error) {
    throw error;
  }
}



/***************************************************************************
                            ADD TASK IN TABLE TASKS
****************************************************************************/
let id_task_added;
async function add_task() {
  let select_priority = document.querySelector('.priority')
  let select_priority_option = select_priority.options[select_priority.selectedIndex].value
  
  
  try {
    let jwtToken = localStorage.getItem("token");
    const response = await fetch("http://localhost:1337/api/tasks", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${jwtToken}`,
      },
      body: JSON.stringify({
        data: {
          task_title: document.querySelector(".task-name").value,
          task_details: document.querySelector(".input-description").value,
          //url: document.querySelector(".url_img_tsk").value,
          satate: document.querySelector(".name-op").innerHTML,
          priority: select_priority_option,
          countDown : document.querySelector('.time-txt').value
        },
      }),
    });

    if (!response.ok) {
      throw new Error("Failed to register project");
    }
    const data = await response.json();
    id_task_added = data.data.id;
    console.log("id_taskadd", id_task_added);
    getTaskAdded(id_task_added);
  } catch (error) {
    throw error;
  }
}

/***************************************************************************
                            SAVE TASK AND RENDER IT           
 **************************************************************************/
document.querySelector(".save").addEventListener("click", async (e) => {
  allTasksProject=[]
  e.preventDefault();
  
  // Add the task and get the ID
  await add_task();

  // Get the tasks of the project
  await getTasks(id_project_active);

  // Get the added task details
  await getTaskAdded(id_task_added);
  

  // Add the task in the project
  console.log('------------------*****************************************');
  console.log(allTasksProject);
  console.log('------------------*****************************************');
  await add_task_inProject(id_project_active, allTasksProject);
  console.log(id_project_active,'id project',allTasksProject,'tasks all bb');

  clear_bord();
  await rendernig_tasks_project(id_project_active)
  
  //console.log(alltas,'alltaskssss youyou');

  // Clear input values and perform other actions
  document.querySelector(".task-name").value = "";
  document.querySelector(".input-description").value = "";
  //document.querySelector(".url_img_tsk").value = "";
  document.querySelector('.users-project').innerHTML=document.querySelector('.users-project').innerHTML=""
  close_task()
  // Check and remove the "No Tasks" element
  if (document.body.contains(document.querySelector(".no-tasks"))) {
    document.querySelector(".no-tasks").remove();
  }
});

/******************************************************************
                        RENDER TASKS
 *****************************************************************/

function renderTasks(data) {
  data.forEach((task) => {
    id_list.push(task["id"]);
    const title = task["attributes"]["task_title"];
    const description = task["attributes"]["task_details"];
    const date = task["attributes"]["createdAt"];
    const url = task["attributes"]["url"];
    const satate = task["attributes"]["satate"].trim();
    const marqup = `
        
                            <p class="page">page<span class="n-p1">122</span>>page<span class="n-p1">1</span></p>
                            <div class="head-task">
                              
                                <h3 class="title-task">${title}</h3>
                                <img class="flag-task" src="assest/images-task/Flag_alt_light.svg" alt="">
                            </div>
                            <!-- <img class="img-task" src="${url}" alt=""> -->
                            <p class="text-task" >${description}</p>
                            <div class="options-task">
                                <div class="option-task">
                                    <p class="nom-option">Ostedhy</p>
                                    <img class="delete-option" src="assest/images-task/fi-rr-cross-small.svg"></img>
                                </div>
                            </div>
                                <div class="cordonne-task">
                                    <div class="cordonne-task-1">
                                        <img src="assest/images-task/Nesting_light.svg" alt="">
                                        <span>6</span>
                                        |
                                        <span>+</span>

                                    </div>
                                    <div class="date-task">
                                        <img src="assest/images-task/Calendar.svg" alt="">
                                        <p>${new Date(
                                          date
                                        ).getDate()}/${new Date(
      date
    ).toLocaleString("default", { month: "short" })}</p>

                                    </div>

                                </div>
                                <div class="footer-task">
                                    <div class="config-users">
                                        <ul class="users">
                                            <li> <img src="assest/images/Avatar.png" alt=""></li>
                                            <li><img src="assest/images/Avatar (1).png" alt=""></li>
                                            <li><img src="assest/images/Avatar (2).png" alt=""></li>
                                            <li><img src="assest/images/Avatar (3).png" alt=""></li>
                                            <li>+5</li>
                        
                                        </ul>
                                        <div class="add-user">
                                            +
                                        </div>
                                    </div>
                                    
                                    <div class="nb-cont">
                                        <img src="assest/images-task/Paperclip.svg" alt="">
                                        <p>4</p>
                                    </div>
                                    <img src="assest/images-task/dots.svg" alt="">

                                </div>
                            
        `;
    const marqup2 = `
    <div class="partie1">
                            <img class="drag" src="assest/create new task/Drag_Vertical.svg" alt>
                            <img class="flech" src="assest/icon list project/fi-rr-caret-right.svg" alt>
                            <img  class="icon-color-task" src="assest/create new task/gris.svg" alt="">
                            <h3 class="title-task">${title}</h3>
                            <div class="cordonne-task-1">
                                <img src="assest/images-task/Nesting_light.svg" alt="">
                                <span>6 |</span>
                            </div>
                        </div>
                        <div class="partie2">
                            <ul class="users">
                                <li><img src="assest/images/Avatar (2).png" alt=""></li>
                                <li><img src="assest/images/Avatar (3).png" alt=""></li>
                                <li>+5</li>
                            </ul>
                            <div class="DueDate-l">
                                ${new Date(
                                  date
                                ).getDate()}/${new Date(
date
).toLocaleString("default", { month: "short" })}
                            </div>
                            <div class="priority-l">
                                <img src="assest/flags/Flag_yellow.svg" alt="">
                            </div>
                            <div class="more-l">...</div>
                          

                        </div>
    
    
    
    
    `                                      


    const new_item = document.createElement("div");
    const new_item2 = document.createElement("div");
    new_item.classList.add("new-task");
    new_item2.classList.add('new-task-l')
    console.log(new_item2)
    new_item.setAttribute("id", task["id"]);
    new_item2.setAttribute("id", task["id"]);

    new_item.setAttribute("draggable", true);
    new_item2.setAttribute("draggable", true);
    new_item.insertAdjacentHTML("beforeend", marqup);
    new_item2.insertAdjacentElement('beforeend',marqup2)
    console.log(new_item2,'heeeeeeeee')

    if (satate == "New Request") {
      document.querySelector(".Request").appendChild(new_item);
      document.querySelector(".request").appendChild(new_item2);
    }
    if (satate == "In Progress") {
      document.querySelector(".inProgress").appendChild(new_item);

    }
    if (satate == "to be Tested") {
      document.querySelector(".tobeTested").appendChild(new_item);
    }
    if (satate == "Completed") {
      document.querySelector(".completed").append(new_item);
    }
  });
}
const clear_bord = function () {
  document.querySelectorAll(".new-task").forEach((task) => {
    task.remove();
    
  });
  document.querySelectorAll(".new-task-l").forEach((task) => {
    task.remove();
    
  });
};

/***********************************************************************
                            UPDATE THE STATE OF TASK
 ***********************************************************************/
async function update_task(id, state) {
  try {
    jwtToken = localStorage.getItem("token");
    const response = await fetch(`http://localhost:1337/api/tasks/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${jwtToken}`,
      },
      body: JSON.stringify({
        data: {
          satate: state,
        },
      }),
    });

    if (!response.ok) {
      throw new Error("Failed to update task");
    }
  } catch (error) {
    throw error;
  }
}
/******************************************************************* */
async function update_task_general(name,description,priority,id ) {
  try {
    jwtToken = localStorage.getItem("token");
    const response = await fetch(`http://localhost:1337/api/tasks/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${jwtToken}`,
      },
      body: JSON.stringify({
        data: {
          task_title:name,
          task_details:description,
          priority:priority,
          
        },
      }),
    });

    if (response.ok) {
      console.log('cbien');
         }

  } catch (error) {
    throw error;
  }
}






/****************************************************************** /
                GET DATA FOR THE NEW TASK ADDED
/****************************************************************** */
async function getTaskAdded(id) {
    try {
      const url = `http://localhost:1337/api/tasks/${id}`;
  
      const response = await fetch(url, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
          "Content-Type": "application/json",
        },
      });
  
      if (response.ok) {
        const data = await response.json();
        console.log(data.data, "new data");
        allTasksProject.push(data.data);
     
      }
    } catch {
      console.log("errrr");
    }
  }
  
/****************************************************************** /
                GET ALL TASKS AND RENDERING IN HTML
/****************************************************************** */
/*async function getAllTask() {
    try {
      const url = `http://localhost:1337/api/tasks?populate=deep,4`;
  
      const response = await fetch(url, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
          "Content-Type": "application/json",
        },
      });
  
      if (response.ok) {
        const data = await response.json();
        console.log(data)

        
        if (data.data.length==0){
          create_noTask()
          document.querySelector('.no-task-btn').addEventListener('click',()=>{
            open_task()
            
          })
        }
        else{
            
            renderTasks(data.data)
            drag()
            document.querySelector('.no-tasks').classList.add('hidden')
            document.querySelector('.project-container').classList.remove('hidden')
            
          


        }
        
  
      } else {
        console.log("Request failed with status:", response.status);
      }
      
    } catch (error) {
      console.log('')
    }
    
  }


/************************************************************
 * GET added task and push in the tasks of selected project
*********************************************************** */
async function deleteTask(id) {
  try {
      const response = await fetch(` http://localhost:1337/api/tasks/${id}`, {
          method: 'DELETE',
          headers: {
              'Content-Type': 'application/json',
              // Add any additional headers if needed, such as authorization headers
          },
      });

      if (!response.ok) {
          throw new Error(`Failed to delete element: ${response.status} - ${response.statusText}`);
      }

      console.log('Element deleted successfully');
  } catch (error) {
      console.error('Error:', error.message);
  }
}




