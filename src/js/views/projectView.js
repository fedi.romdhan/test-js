let taskID
drag = function () {
  const lists = document.querySelectorAll(".new-task");
  const newreq = document.querySelector(".Request");
  const progress = document.querySelector(".inProgress");
  const tobeTested = document.querySelector(".tobeTested");
  const completed = document.querySelector(".completed");
  for (let list of lists) {
    list.addEventListener("dragstart", function (e) {
      let selected = e.target.closest(".new-task");
      selected.style.border = " 2px dashed black";
      newreq.addEventListener("dragover", function (e) {
        e.preventDefault();
        selected.style.border = "";
      });
      newreq.addEventListener("drop", function (e) {
        newreq.appendChild(selected);
        update_task(selected.id, "New Request");
        clear_bord()
        rendernig_tasks_project(projectID)
        selected = null;
        document.querySelector('.users-project').innerHTML=''
        
       
      
      });

      tobeTested.addEventListener("dragover", function (e) {
        e.preventDefault();
        selected.style.border = "";
      });
      tobeTested.addEventListener("drop", function (e) {
        tobeTested.appendChild(selected);
        update_task(selected.id, "to be Tested");
        clear_bord()
        rendernig_tasks_project(projectID)
        selected = null;
        document.querySelector('.users-project').innerHTML=''
      });

      progress.addEventListener("dragover", function (e) {
        e.preventDefault();
        selected.style.border = "";
      });
      progress.addEventListener("drop", function (e) {
        progress.appendChild(selected);
        update_task(selected.id, "In Progress");
        clear_bord()
        rendernig_tasks_project(projectID)
        selected = null;
        document.querySelector('.users-project').innerHTML=''
      });

      completed.addEventListener("dragover", function (e) {
        e.preventDefault();
        selected.style.border = "";
      });
      completed.addEventListener("drop", function (e) {
        completed.appendChild(selected);
        update_task(selected.id, "Completed");
        clear_bord()
        rendernig_tasks_project(projectID)
        selected = null;
        document.querySelector('.users-project').innerHTML=''
      });
    });
  }
};

dragL = function () {
  
  const lists = document.querySelectorAll(".new-task-l");
  const newreq = document.querySelector(".request");
  const progress = document.querySelector(".inprogress");
  const tobeTested = document.querySelector(".tobetested");
  const completed = document.querySelector(".Completed");
  for (let list of lists) {
    list.addEventListener("dragstart", function (e) {
      let selected = e.target.closest(".new-task-l");
      selected.style.border = "2px dashed black";
      
      newreq.addEventListener("dragover", function (e) {
        e.preventDefault();
        selected.style.border = "";
      });
      newreq.addEventListener("drop", function (e) {
        newreq.appendChild(selected);
        update_task(selected.id, "New Request");
        selected.querySelector(".icon-color-task").src =
          "assest/create new task/gris.svg";
          clear_bord()
        rendernig_tasks_project(projectID)
        document.querySelector('.project-container').classList.add('hidden')
        //document.querySelector('.project-container').classList.add('hidden')
        selected = null;
        document.querySelector('.users-project').innerHTML=''
      });

      tobeTested.addEventListener("dragover", function (e) {
        e.preventDefault();
        selected.style.border = "";
      });
      tobeTested.addEventListener("drop", function (e) {
        tobeTested.appendChild(selected);
        selected.querySelector(".icon-color-task").src =
          "assest/create new task/blue.svg";
        update_task(selected.id, "to be Tested");
        clear_bord()
        rendernig_tasks_project(projectID)
        selected = null;
        document.querySelector('.users-project').innerHTML=''
      });

      progress.addEventListener("dragover", function (e) {
        e.preventDefault();
        selected.style.border = "";
      });
      progress.addEventListener("drop", function (e) {
        progress.appendChild(selected);
        selected.querySelector(".icon-color-task").src =
          "assest/create new task/orange.svg";
        update_task(selected.id, "In Progress");
        clear_bord()
        rendernig_tasks_project(projectID)
        selected = null;
        document.querySelector('.users-project').innerHTML=''
      });

      completed.addEventListener("dragover", function (e) {
        e.preventDefault();
        selected.style.border = "";
      });
      completed.addEventListener("drop", function (e) {
        completed.appendChild(selected);
        selected.querySelector(".icon-color-task").src =
          "assest/create new task/vert.svg";
        update_task(selected.id, "Completed");
        clear_bord()
        rendernig_tasks_project(projectID)
        selected = null;
        document.querySelector('.users-project').innerHTML=''
      });
    });
  }
};
function active_list(){
  document.querySelector('.project-container').classList.add('hidden')
  document.querySelector('.project-conatiner-list').classList.remove('hidden')
  console.log(document.querySelector('.add-tag-btn'),'waaaahit');
}

async function registerProject() {
  try {
    jwtToken = localStorage.getItem("token");
    const response = await fetch("http://localhost:1337/api/projects", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${jwtToken}`,
      },
      body: JSON.stringify({
        data: {
          name: document.querySelector(".c-project-name").value,
          url: document.querySelector(".c-project-img").value,
        },
      }),
    });
    if (!response.ok) {
      throw new Error("Failed to register project");
    }
  } catch (error) {
    throw error;
  }
}
/************************************************************************************
                    RENDER PROJECT
 ************************************************************************************/
const renderProject = function (data) {



  data.forEach((project) => {
    const name = project["attributes"]["name"];
    const url = project["attributes"]["url"];
    const id = project["id"];
    const favorite = project["attributes"]["favorite"]
    

    const marq = `
    
        <div class="header-item active-project">
        <div class="item-list-project-p1">
        <img class="flech" src="assest/icon list project/fi-rr-caret-right.svg" alt=""> 
        <img class="img-project" src="${url}" alt="">
        <p class="tit-project" >${name}</p>
        </div>
        <div class="item-list-project-p2">
        <img src="assest/icon list project/dots (1).svg" alt="">
        
        </div>
        </div>
    `;

    let add_folder = document.createElement("span");
    add_folder.insertAdjacentHTML("afterbegin", "+");
    add_folder.classList.add("add-folder");

    let folder = document.createElement("ul");
    folder.classList.add("folders");
    folder.classList.add("close-folders");
    folder.classList.add("notVis");
    let marq_folder = `<li>
    <div class="folder">
    <div class="folder-p1">
        <img src="assest/icon list project/Vector (1).svg
        
        " alt="">
        <img src="assest/icon list project/Vector (3).svg" alt="">
        <p>Social Media</p>
    </div>
    <div class="folder-p2">
        <img src="assest/icon list project/dots (1).svg" alt="">
        <span class="add-folder folder" >+</span>
    </div>
    </div></li>`;
    folder.insertAdjacentHTML("beforeend", marq_folder);

    const newLi = document.createElement("li");
    newLi.setAttribute("id", id);
    newLi.setAttribute("favorite",favorite)
    newLi.classList.add("item-list-project");
    newLi.insertAdjacentHTML("afterbegin", marq);
    newLi.querySelector(".item-list-project-p2").appendChild(add_folder);
    newLi.appendChild(folder);
    document.querySelector(".list-project").appendChild(newLi);


    const newLif = document.createElement("li");
    newLif.setAttribute("id", id);
    newLif.classList.add("item-list-project");
    newLif.insertAdjacentHTML("afterbegin", marq);
    newLif.querySelector(".item-list-project-p2").appendChild(add_folder);
    newLif.appendChild(folder);
    

 

      
    if (favorite==true){  
    document.querySelector('.list-project-favorits').appendChild(newLif)

    
    }

});
}
/***************************************************************
                        TOGGLE PROJET   
/**************************************************************/
const toggle = function () {
  document.querySelectorAll(".flech").forEach((flech) => {
    flech.addEventListener("click", () => {
      flech.classList.toggle("flech-down");
      let folders =
        flech.parentNode.parentElement.parentElement.querySelector(".folders");
      folders.classList.toggle("close-folders");
    });
  });
};

/*******************************************************************
                        ADD FOLDER  
/******************************************************************/
const addFolder = function () {
  document.querySelectorAll(".add-folder").forEach((add) => {
    add.addEventListener("click", () => {
      let folders =
        add.parentNode.parentNode.parentNode.querySelector(".folders");
      folders.classList.remove("close-folders");
      folders.classList.remove("notVis");
      let one_folder = document.createElement("li");
      let marq = `<div class="folder">
                <div class="folder-p1">
                <img src="assest/icon list project/Vector (1).svg                                 
                " alt="">
                <img src="assest/icon list project/Vector (3).svg" alt="">
                <p>Social Media</p>
                </div>
                <div class="folder-p2">
                <img src="assest/icon list project/dots (1).svg" alt="">
                <span class="add-folder folder-second" >+</span>
                </div>
            </div>`;
      one_folder.insertAdjacentHTML("afterbegin", marq);
      folders.appendChild(one_folder);
    });
  });

  
};

/*****************************************************************
                RENDER TASKS FOR ONE PROJECT
******************************************************************/
/*importiha*/
let allTaskss;
let comments
function renderTasks(data) {
  
  let nb_req = (nb_pro = nb_tes = nb_com = 0);
  data.forEach((task) => {
    console.log(task,'honnnna lcommment');
    const title = task["attributes"]["task_title"];
    const description = task["attributes"]["task_details"];
    const date = task["attributes"]["createdAt"];
    const url = task["attributes"]["url"];
    const priority = task["attributes"]["priority"];
    let flag;
    if (priority === "high") {
      flag = "red";
    }
    if (priority === "average") {
      flag = "yellow";
    }
    if (priority === "low") {
      flag = "blue";
    }
    const satate = task["attributes"]["satate"].trim();
    let status_icon;

    if (satate == "New Request") {
      status_icon = `<img class="icon-color-task" src="assest/create new task/gris.svg" alt="">`;
      nb_req++;
    }
    if (satate == "In Progress") {
      status_icon = `<img class="icon-color-task" src="assest/create new task/orange.svg" alt="">`;
      nb_pro++;
    }
    if (satate == "to be Tested") {
      status_icon = `<img class="icon-color-task" src="assest/create new task/blue.svg" alt="">`;
      nb_tes++;
    }
    if (satate == "Completed") {
      status_icon = `<img class="icon-color-task" src="assest/create new task/vert.svg" alt="">`;
      nb_com++;
    }
    let img=""
    console.log(url,'urrrrrlll');
    if (url){
       img = `<img class="img-task" src="${url}" alt="">`
    }
    
    const marqup = `
        
        <div class="head-task">
          <h3 class="title-task">${title}</h3>
          <img class="flag-task" src="assest/flags/Flag_${flag}.svg" alt="">
        </div>
        ${img}
          
        <p class="text-task" >${description}</p>
        <div class="options-task">
        <div class="option-task">
        <p class="nom-option">Ostedhy</p>
        <img class="delete-option" src="assest/images-task/fi-rr-cross-small.svg"></img>
        </div>
        <div class="option-task c2">
        <p class="nom-option">marketing</p>
        <img class="delete-option" src="assest/images-task/fi-rr-cross-small.svg"></img>
        </div>
        <div class="option-task c3">
        <p class="nom-option">Ostedhy</p>
        <img class="delete-option" src="assest/images-task/fi-rr-cross-small.svg"></img>
        </div>
        
        </div>
        <div class="cordonne-task">
          <div class="cordonne-task-1">
            <img src="assest/images-task/Nesting_light.svg" alt="">
              <span>6</span>
                |
                <span>+</span>
                </div>
        <div class="date-task">
          <img src="assest/images-task/Calendar.svg" alt="">
            <p>${new Date(date).getDate()}/${new Date(date).toLocaleString("default", { month: "short" })}</p>
              </div>
              </div>
             <div class="footer-task">
            <div class="config-users">
            <ul class="users">
            ${document.querySelector('.users-project').innerHTML}

              </ul>
              <div class="add-user">
                  +
  </div>
            </div>
                                    
              <div class="nb-cont">
              <img src="assest/images-task/Paperclip.svg" alt="">
                  <p>4</p>
              </div>
              <img src="assest/images-task/dots.svg" alt="">
                </div>                   
        `;

    const marqup2 = `
        <div class="partie1">
                                <img class="drag" src="assest/create new task/Drag_Vertical.svg" alt>
                                <img class="flech" src="assest/icon list project/fi-rr-caret-right.svg" alt>
                                ${status_icon}
                                <h3 class="title-task">${title}</h3>
                                <div class="cordonne-task-1">
                                    <img src="assest/images-task/Nesting_light.svg" alt="">
                                    <span>6</span>
                                    <span>|</span>
                                 
        <div class="options-task">
        <div class="option-task">
        <p class="nom-option">Ostedhy</p>
        <img class="delete-option" src="assest/images-task/fi-rr-cross-small.svg"></img>
        </div>
        <div class="option-task c2">
        <p class="nom-option">marketing</p>
        <img class="delete-option" src="assest/images-task/fi-rr-cross-small.svg"></img>
        </div>
        <div class="option-task c3">
        <p class="nom-option">Ostedhy</p>
        <img class="delete-option" src="assest/images-task/fi-rr-cross-small.svg"></img>
        </div>
        </div>
          <img class="add-tag-btn" src="assest/tag.svg" alt="">



                                </div>
                            </div>
                            <div class="partie2">
                            <ul class="users">
                              ${document.querySelector('.users-project').innerHTML}
                                
                                    
                                </ul>
                                <div class="DueDate-l">
                                ${new Date(date).getDate()}/${new Date(
      date
    ).toLocaleString("default", { month: "short" })}
                                </div>
                                <div class="priority-l">
                                    <img src="assest/flags/Flag_${flag}.svg" alt="">
                                </div>
                                <div class="more-l">...</div>
                              
    
                            </div>
        
        
        
        
        `;
    const new_item = document.createElement("div");
    const new_item2 = document.createElement("div");
    new_item.classList.add("new-task");
    new_item2.classList.add("new-task-l");
    new_item.setAttribute("id", task["id"]);
    new_item2.setAttribute("id", task["id"]);
    new_item.setAttribute("draggable", true);
    new_item2.setAttribute("draggable", true);
    new_item.insertAdjacentHTML("beforeend", marqup);
    new_item2.insertAdjacentHTML("beforeend", marqup2);

    if (satate == "New Request") {
      document.querySelector(".Request").appendChild(new_item);
      document.querySelector(".request").appendChild(new_item2);
    }
    if (satate == "In Progress") {
      document.querySelector(".inProgress").appendChild(new_item);
      document.querySelector(".inprogress").appendChild(new_item2);
    }
    if (satate == "to be Tested") {
      document.querySelector(".tobeTested").appendChild(new_item);
      document.querySelector(".tobetested").appendChild(new_item2);
    }
    if (satate == "Completed") {
      document.querySelector(".completed").append(new_item);
      document.querySelector(".Completed").append(new_item2);
    }
  });
  console.log(nb_req, nb_pro, nb_tes, nb_com);
  document.querySelector(".nb-req").innerHTML = nb_req;
  document.querySelector('.nb-req-l').innerHTML=nb_req;
  document.querySelector(".nb-pro").innerHTML = nb_pro;
  document.querySelector(".nb-pro-l").innerHTML = nb_pro;
  document.querySelector(".nb-tes").innerHTML = nb_tes;
  document.querySelector(".nb-tes-l").innerHTML = nb_tes;
  document.querySelector(".nb-com").innerHTML = nb_com;
  document.querySelector(".nb-com-l").innerHTML = nb_com;
   allTaskss = [
    ...document.querySelectorAll(".new-task"),
    ...document.querySelectorAll(".new-task-l"),
  ];
  console.log(allTaskss);
  allTaskss.forEach((task) => {
    task.addEventListener("click", async (e) => {
      e.preventDefault();
      
      taskID=task.id
      
      try {
        const url = `http://localhost:1337/api/tasks/${task.id}?populate=comments.users,assignees,attachments`;

        const response = await fetch(url, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "application/json",
          },
        });

        if (response.ok) {
          let data = await response.json();
          data = data.data
          comments=data.attributes.comments.data
          
          rendering_comment(comments)
          console.log(comments,'all comeeeeeennet');
          console.log(data, "data for task open", task.id);
          open_update_task()
          document.querySelector('.tag-principal-update').addEventListener('click',()=>{
            
          })
          
          document.querySelector("time")
          document.querySelector(".task-name-update").value =data.attributes.task_title
          document.querySelector('.input-description-update').value=data.attributes.task_details
          setTimeout(()=>{
          let deadline = new Date(data.attributes.countDown)
          let currentDate = new Date()
          let timeDifference = deadline -currentDate
          const days = Math.floor(timeDifference / (1000 * 60 * 60 * 24));
          const hours = Math.floor((timeDifference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          const minutes = Math.floor((timeDifference % (1000 * 60 * 60)) / (1000 * 60));
          console.log('min',minutes,"hou",hours,"day",days  );
          let date_txt=`${days}day/${hours}h/${minutes}min`
          if (days<=0 && hours<=0 && minutes<=0){
            document.querySelector('.time-txt-update').innerHTML="Time Out"
          }

          else
          {document.querySelector('.time-txt-update').innerHTML=date_txt}},1000)
          let select_priority = document.querySelector(".priority-update");
            select_priority.value = data.attributes.priority;
            console.log(select_priority.value);
            if (select_priority.value === "average") {
              document.querySelector(".type-flag-update").src =
                "assest/flags/Flag_yellow.svg";
            }
            if (select_priority.value === "high") {
              document.querySelector(".type-flag-update").src =
                "assest/flags/Flag_red.svg";
            }
            if (select_priority.value === "low") {
              document.querySelector(".type-flag-update").src =
                "assest/flags/Flag_blue.svg";
            }
            
      
      const updateButton = document.querySelector('.update');
      const deleteButton = document.querySelector('.delete');

      updateButton.removeEventListener('click', updateTaskHandler);
      deleteButton.removeEventListener('click', deleteTaskHandler);

      
      updateButton.addEventListener('click', updateTaskHandler);
      deleteButton.addEventListener('click', deleteTaskHandler);


        }
      } catch {
        console.log("errrr");
      }
    });
  });
  
}
/***********************************************************************
 *      RENDER TASKS FOR ONE PROJECT
 **********************************************************************/
const rendernig_tasks_project = async (projectId) => {
  
  
  if (document.querySelector('.list_container').classList.contains('project-play-active')==true){
    document.querySelector('.project-container').classList.add('hidden')
    

  }
  
  if (document.body.contains(document.querySelector(".no-tasks"))) {
    document.querySelector(".no-tasks").remove();
  }
  try {
    const res = await fetch(
      `http://localhost:1337/api/projects/${projectId}?populate=*`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (!res.ok) {
      throw new Error("Failed to getTasks ");
    }
    const { data } = await res.json();
    if (data.attributes) {
      const len_data = data.attributes.tasks.data;

      
      document.querySelector(".personne-assignes").innerHTML=""
      render_user_assignees(data.attributes.users_permissions_users.data)
      if (len_data != 0) {
        allTasksProject = data.attributes.tasks.data;
        console.log(allTasksProject);
        clear_bord()
        renderTasks(allTasksProject);
      
        drag();
        dragL();
        document.querySelector(".project-container").classList.remove("hidden");
        
        //return allTasksProject;
      } else {
        create_noTask();
        document.querySelector(".project-container").classList.add("hidden");
        document
          .querySelector(".project-container-list")
          .classList.add("hidden");

        
        document.querySelector(".no-task-btn").addEventListener("click", () => {
          open_task();
        });
      }
    }
  } catch (error) {
    console.log(error);
  }
};
/************************************************************************************************/
let allTasksProject;
let allUserProject;
const getTasks = async (projectId) => {
  
  try {
    const res = await fetch(
      `http://localhost:1337/api/projects/${projectId}?populate=*`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (!res.ok) {
      throw new Error("Failed to getTasks ");
    }
    const { data } = await res.json();
    if (data.attributes) {
      const len_data = data.attributes.tasks.data;
      if (len_data != 0) {
        allTasksProject = data.attributes.tasks.data;
        //renderTasks(data.attributes.tasks.data)
        drag();
        dragL();
      } else {

        document.querySelector(".no-task-btn").addEventListener("click", () => {
          open_task();
        });
        create_noTask();
      }
    }
  } catch (error) {
    console.log(error);
  }
};
/**************************************************************************************
        ACTIVE PROJECT
 *************************************************************************************/

const remove_active_item = function () {
  document.querySelectorAll(".header-item").forEach((item) => {
    item.classList.remove("header-item-active");
    item.querySelector('.tit-project').style.color="#374957"
    document.querySelector('.project-container').classList.remove('hidden')
    document.querySelector('.project-container').style.display="block"
    
  
    //document.querySelector('.no-tasks').
  });
};
let projectID
const activeProject = function () {
  
  
  document.querySelectorAll(".header-item").forEach((item) => {
    item.addEventListener("click", async() => {
      console.log("********************************************************");
      console.log(item.parentNode.getAttribute('favorite'))
      console.log(item.parentNode.getAttribute('favorite')==='true');
      console.log("********************************************************");

      if(item.parentNode.getAttribute('favorite')=='true'){
        document.querySelector('.btn-fav').src="assest/star.png"

      }
      else{
        document.querySelector('.btn-fav').src="assest/icon/star.svg"
      }
      document.querySelector('.users-project').innerHTML=''
      document.querySelector('.btn-fav').addEventListener('click',async()=>{
        document.querySelector('.btn-fav').src="assest/star.png"
        
        await favorite_Project(id_project_active)
        await getAllProject_user()
        document.querySelector('.list-project-favorits').innerHTML=""
        document.querySelector('.list-project').innerHTML=""  
        renderProject(datas)
        activeProject();
        addFolder();
        toggle();
        addSecondFolder();
       
        
        




      })
      remove_active_item();
      item.classList.add("header-item-active");
      item.querySelector('.tit-project').style.color="#fff"
      id_project_active = item.parentElement.id;

      projectID=id_project_active
      await getUsers_project(id_project_active)
      let personnes=document.querySelector('.personnes')
      let divs = personnes.getElementsByTagName('div')
      let divArray = Array.from(divs);
      divArray.forEach((div)=>{
        div.remove()  
      })

      await getUsers()

      console.log(allUserProject,'//////////////first users///////////////////////');
      

      let projectName =
      item.parentElement.querySelector(".tit-project").innerHTML;
      document.querySelector(".project-name-title").innerHTML = projectName; 
      rendernig_tasks_project(id_project_active)
        document.querySelectorAll('.one-user').forEach(async(user)=>{
          
          user.addEventListener('click',async()=>{
            user.remove()
            document.querySelector('.personne-assignes').appendChild(user)
            
            console.log("id p: ",id_project_active,'id: ',user.id)
            await getUser(user.id)
            console.log(allUserProject ,'hiya   hiyya hiyyya');
            await   addUserInProject(id_project_active,allUserProject)
            
            


            
          
          
          
          
          })

        })

      })
    
    })}

////////////////////////////////////////////////////////////////////////////////////
//                THE PROJECTS FOR ONE USER
///////////////////////////////////////////////////////////////////////////////////
let datas
const getAllProject_user = async function () {
  let id_user=localStorage.getItem('id')
  try {

    const url = `http://localhost:1337/api/users/${id_user}?populate=*`;
    const response = await fetch(url, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
    });
    if (response.ok) {
      const data = await response.json();
      //renderProject(data);
      let projects = data.projects
      console.log(projects);
      datas=[]
      projects.forEach((project)=>{
        let obj ={
          id:project.id,
          attributes:{...project}
        }
        datas.push(obj)
      })
      
        
      renderProject(datas)
      activeProject();
      addFolder();
      toggle();
      addSecondFolder();
      
    } else {
      throw error;
    }
  } catch (error) {
    console.log("erro");
  }
};
 getAllProject_user();








 function updateTaskHandler() {
  document.querySelector('.users-project').innerHTML = '';
  const title = document.querySelector(".task-name-update").value;
  const details = document.querySelector('.input-description-update').value;
  let select_priority = document.querySelector(".priority-update");

  const priority = select_priority.value;  
  console.log(title, details, priority, 'task: ', taskID);
  update_task_general(title, details, priority, taskID);
  close_task_update();
  console.log(projectID, 'projectid');
  clear_bord();
  rendernig_tasks_project(projectID);
  console.log(allTasksProject, 'chouf lena');

} 

 function deleteTaskHandler() {
  deleteTask(taskID);
  close_task_update();
  clear_bord();
  rendernig_tasks_project(projectID);
  document.querySelector('.users-project').innerHTML = '';
}
/***********************************
 * favorite project
 */
async function favorite_Project(projectId){
  try {
      jwtToken = localStorage.getItem("token");
      const response = await fetch(`http://localhost:1337/api/projects/${projectId}`, {
      method: "PUT",
      headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${jwtToken}`,
      },
      body: JSON.stringify({
          data: {
          favorite: true
          },
      }),
      });

      if (!response.ok) {
      throw new Error("Failed to update task");
      }
  } catch (error) {
      throw error;
  }


}
