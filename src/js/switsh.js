document.addEventListener("DOMContentLoaded", function () {
  /*if(!id_project_active){
    return 0
  }*/ 
    // Get the list and board elements
    const list = document.querySelector('.list_container');
    const board = document.querySelector('.board_container');

    // Check if there is a saved state in localStorage
    const savedState = localStorage.getItem('pageState');

    if (savedState === 'list') {
      // If the saved state is 'list', display the list
      document.querySelector('.project-container-list').style.display = 'block';
      document.querySelector('.project-container').style.display = 'none';
    } else {
      // If the saved state is not 'list', display the board (default)
      document.querySelector('.project-container-list').style.display = 'none';
      document.querySelector('.project-container').style.display = 'block';
    }

    // Save the state when switching between list and board
    list.addEventListener('click', function () {
        document.querySelector('.project-container-list').style.display = 'block';
        document.querySelector('.project-container').style.display = 'none';
    
      localStorage.setItem('pageState', 'list');
    });

    board.addEventListener('click', function () {
        document.querySelector('.project-container-list').style.display = 'none';
        document.querySelector('.project-container').style.display = 'block';
      localStorage.setItem('pageState', 'board');
    });
  })
