let IDuser
let nameUser
if (!localStorage.getItem("token")) window.location.href = "Login.html";
else {
  const BASE_URL = "http://localhost:1337";
  const getme = async () => {
    const jwtToken = localStorage.getItem("token");
    const res = await fetch(`${BASE_URL}/api/users/me`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${jwtToken}`,
      },
    });
    const data = await res.json();
    
    console.log(data.id,'moi');
    IDuser=data.id
    localStorage.setItem('id',data.id)
    console.log(IDuser,'me');
    console.log(data,'for me');
    document.querySelector('.avatar-user-profile').src=data.url_avatar
    nameUser=data.username
   

    return data;
  };
  getme()
  console.log((IDuser,"2222mois"));
 
}

document.addEventListener('DOMContentLoaded',async()=>{
  await getAllProject_user()
  await getAllProject_user()
  document.querySelector('.list-project-favorits').innerHTML=""
  document.querySelector('.list-project').innerHTML=""  
  renderProject(datas)
  activeProject();
  addFolder();
  toggle();
  addSecondFolder();
})

let select = document.querySelector('.priority')
select.addEventListener("change", function() {
  let selected_flag = select.options[select.selectedIndex];
  console.log(selected_flag.value,'yellow')
  console.log(selected_flag.value=='yellow')
  if(selected_flag.value=='average'){
    document.querySelector('.type-flag').setAttribute('src',"assest/flags/Flag_yellow.svg")
  }
  if(selected_flag.value=='low'){
    document.querySelector('.type-flag').setAttribute('src',"assest/flags/Flag_blue.svg")
  }
  if(selected_flag.value=='high'){
    document.querySelector('.type-flag').setAttribute('src',"assest/flags/Flag_red.svg")
  }

})


/****************************************************** */

/*********************************************************
          SWITCH BEETWEEN LIST AND BOARD
 *********************************************************/
function remove_active_play(){
  document.querySelectorAll('.project-play-item').forEach((item)=>{
    item.classList.remove('project-play-active')
  
})

}

document.querySelectorAll('.project-play-item').forEach((item)=>{
    item.addEventListener('click',()=>{
      remove_active_play()
      console.log(!id_project_active)
      if(!id_project_active){
        document.querySelector('.project-container-list').classList.add('hidden')
        document.querySelector('.project-container').classList.add('hidden')
        return 0
      }
      if (document.querySelector('.project-container-list').classList.contains('hidden')){
        document.querySelector('.project-container-list').classList.remove('hidden')
        document.querySelector('.project-container').classList.add('hidden')
        return 0
      }
      else{
        console.log("nooooooo");
      }
      
      item.classList.add('project-play-active')
      if (item.classList.contains('list_container')){
        document.querySelector('.project-container-list').classList.remove('hidden')
        document.querySelector('.project-container').classList.add('hidden')

      }
      else{
        document.querySelector('.project-container-list').classList.add('hidden')
        document.querySelector('.project-container').classList.remove('hidden')

      }
    })
}
  
)

/***********************************************************************
                            LOGOUT 
 **********************************************************************/

document.querySelector(".logout").addEventListener("click", () => {
  localStorage.removeItem("token");
  window.location.href = "LogIn.html";
});



///add folder second
const second =`<ul class="second-list-folders">
  <li class="folder second-folder">
      <div class="folder-p1">
      <img src="assest/icon list project/u_subject.svg                              
       " alt="">
      <img src="assest/icon list project/Vector (3).svg" alt="">
        <p>Planning </p>
        </div>
      <div class="folder-p2">
        <img src="assest/icon list project/dots (1).svg" alt="">
        <span class="add-folder add-folder-second" >+</span>
        </div>
  </li>

  
  </ul>`


/*********************************************************************************
           open model new Project
 *********************************************************************************/
document.querySelector('.new-project').addEventListener('click',()=>{
  document.querySelector('.model-create-project').classList.remove('hidden')
  document.querySelector('.container').classList.add('container-blur')
})

document.querySelector('.btn-create-project').addEventListener('click',()=>{
  var name = document.querySelector('.c-project-name').value
  var url = document.querySelector('.c-project-img').value
  if (name!="" && url!=""){
    console.log(name!="" && url!="")
    const marqup = ` 
    <li class="item-list-project ">
      <div class="header-item active-project">
        <div class="item-list-project-p1">
          <img class="flech" src="assest/icon list project/fi-rr-caret-right.svg" alt=""> 
          <img class="img-project" src="${url}" alt="">
          <p class="tit-project" >${name}</p>
       </div>
       <div class="item-list-project-p2">
          <img src="assest/icon list project/dots (1).svg" alt="">
           <span class="add-folder" >+</span>
        </div>
      </div>
       <ul class="folders close-folders notVis">
       </ul>
    </li>
  `
  document.querySelector('.list-project').insertAdjacentHTML('beforeend',marqup)
  registerProject()
  document.querySelector('.c-project-name').value=""
  var url = document.querySelector('.c-project-img').value="" 
}

})
/*********************************************************************************
           close model new project
 ********************************************************************************/
document.querySelector('.btn-create-project').addEventListener('click',()=>{
document.querySelector('.model-create-project').classList.add('hidden')
document.querySelector('.container').classList.add('container-blur')
})

document.querySelector('.close-icon-model').addEventListener('click',()=>{
  document.querySelector('.model-create-project').classList.add('hidden')
  document.querySelector('.container').classList.remove('container-blur')
  })
/********************************************************************** */

  



/******************************************************************************
             create noTask model
 ******************************************************************************/
function create_noTask(){
  const el = document.createElement('div');
  el.className = 'no-tasks'; // Add a class
  el.insertAdjacentHTML('afterbegin',`<img src="assest/EmptyState.png" alt="">
  <div>
  <h3>No Tasks</h3>
  <p>Start creating your tasks</p>
</div>
  <div class="no-task-btn">Create a new Task</div>`
)
  document.querySelector('.project').appendChild(el)
  document.querySelector('.project-container').classList.add('hidden')
  
}






/*********************************************************************************
                            select creation
*********************************************************************************/
document.querySelector('.lota').addEventListener('click',()=>{
  console.log('lota')
  if (document.querySelector('.task-options').classList.contains('hidden')){
    
  document.querySelector('.lota').style.transform="rotate(-90deg)"}
  else{
    document.querySelector('.lota').style.transform="rotate(0deg)"
  }
  document.querySelector('.task-options').classList.toggle('hidden')

});
let name_op = document.querySelector('.name-op')

const options_name = ["New Request","In Progress","to be Tested","Completed"]

const colors = ["#c5c5c5ce","#FFA948","#0BA5EC","#3DD455"]
let options=document.querySelectorAll(".task-create-option")
options.forEach((op)=>{
  op.addEventListener('click',()=>{
    console.log(op)
    if(op.classList.contains('op1')){
      document.querySelector('.selected').style.backgroundColor=colors[0]
      

      const newLi = `
      <li  class="task-create-option op${options_name.indexOf(name_op)}">
      <img src="assest/create new task/blue.svg" alt="">
      <p  >${name_op.innerHTML}</p>
      </li>
      
      `
      document.querySelector(".task-options").insertAdjacentHTML('afterbegin',newLi)
      console.log(newLi)
      name_op.innerHTML=name_op.innerHTML.replace(name_op.innerHTML,options_name[0])
      op.classList.add('hidden')

      
    }

    if(op.classList.contains('op2')){
      document.querySelector('.selected').style.backgroundColor=colors[2]
      const newLi = `
      <li  class="task-create-option op${options_name.indexOf(name_op)}">
      <img src="assest/create new task/blue.svg" alt="">
      <p  >${name_op.innerHTML}</p>
      </li>
      
      `
      document.querySelector(".task-options").insertAdjacentHTML('afterbegin',newLi)
      name_op.innerHTML=name_op.innerHTML.replace(name_op.innerHTML,options_name[2])
      op.classList.add('hidden')
    } 

    if(op.classList.contains('op3')){
      document.querySelector('.selected').style.backgroundColor=colors[3]
      name_op.innerHTML=name_op.innerHTML.replace(name_op.innerHTML,options_name[3])
      const newLi = `
      <li  class="task-create-option op${options_name.indexOf(name_op)}">
      <img src="assest/create new task/blue.svg" alt="">
      <p  >${name_op.innerHTML}</p>
      </li>
      
      `
      document.querySelector(".task-options").insertAdjacentHTML('afterbegin',newLi)
      name_op.innerHTML=name_op.innerHTML.replace(name_op.innerHTML,options_name[3])
      op.classList.add('hidden')
    }
    options=document.querySelectorAll(".task-create-option")
    document.querySelector('.task-options').classList.add('hidden')
  })
})
/****************************************************************************/
/*********************************************************************************
                            CHANGE TYPE-FLAG
*********************************************************************************/   
document.querySelector('.show').addEventListener('click',()=>{
  document.querySelector('.list-project').classList.toggle('hidden')
  if (document.querySelector('.list-project').classList.contains('hidden')){
    document.querySelector('.show').style.transform='rotate(-90deg)'
  }
  else{
    document.querySelector('.show').style.transform='rotate(0deg)'
  


  

}})

/*********************************************************************************
                            UI
*********************************************************************************/  
function remove_active_navbar(){
  let all = document.querySelectorAll('.nav-item')
  all.forEach(nav=>{nav.classList.remove('nav-active')
  })

}
document.querySelectorAll('.nav-item').forEach((nav)=>{
  nav.addEventListener("click",(e)=>{
    e.preventDefault( )
    remove_active_navbar()
    nav.classList.add('nav-active')

  })
  

})
/*********************************************************************************
                            UI
*********************************************************************************/  
/*ocument.querySelector('.add-tag-btn').addEventListener('click',()=>{
  console.log(document.querySelector('.add-tag-btn').parentElement,'00000000000000000000000000000');
 
  alert('hello ')
})*/
