
/**********************************************************************
                                ADD COMMENT
 **********************************************************************/
    async function add_comment(comment_text, users, task) {
        let jwtToken = localStorage.getItem("token");
        try {
            let response = await fetch("http://localhost:1337/api/comments", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${jwtToken}`,
                },
                body: JSON.stringify({
                    data: {
                        comment_text: comment_text,
                        users_permissions_user: users,
                        task: task,
                        user:nameUser
                    },
                }),
            });
    
            if (!response.ok) {
                throw new Error("Failed to register comment");
            }
    
            // Handle success if needed
    
        } catch (error) {
            console.error('Error:', error.message);
        }
    }
    
    document.addEventListener('DOMContentLoaded', () => {
        document.querySelector('.envoyer-update').addEventListener('click', async () => {
            const comment_text = document.querySelector(".input-comment-update").value;
            const users_permissions_user = localStorage.getItem('id');
            const task = taskID ;
            console.log(comment_text, task, users_permissions_user);
            await add_comment(comment_text, users_permissions_user, task);
            let divcoment=document.createElement('div')
            divcoment.classList.add("header-comment-update")
            let marq = `
            
                <img src="assest/create new task/Vector (4).svg" alt="">
            <div class="coment">
                <p class="user-comment">${nameUser}</p> 
                <p>${comment_text}</p>
            </div>
                <span>jsut now</span>
        
            `
        divcoment.insertAdjacentHTML('beforeend',marq)
        document.querySelector('.comments-update').appendChild(divcoment)
        console.log({divcoment});
            document.querySelector(".input-comment-update").value=""
        });
    });



   
    


function rendering_comment(data){
    document.querySelector('.comments-update').innerHTML=""
    data.forEach(comment => {
        console.log(comment);
        let divcoment=document.createElement('div')
        divcoment.classList.add("header-comment-update")
        let marq = `
        
            <img src="assest/create new task/Vector (4).svg" alt="">
        <div class="coment">
            <p class="user-comment">${comment.attributes.user}</p> 
            <p>${comment.attributes.comment_text}</p>
        </div>
            <span>${new Date(
                comment.attributes.createdAt
              ).getDate()}/${new Date(
                comment.attributes.createdAt
).toLocaleString("default", { month: "short" })}</span>
       
        `
        divcoment.insertAdjacentHTML('beforeend',marq)
        document.querySelector('.comments-update').appendChild(divcoment)
        console.log({divcoment});
        
    });
}